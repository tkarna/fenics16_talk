#!/bin/bash
INFILE=/home/tuomas/Documents/references/aa_citations/all_merged.bib
OUTFILE=references.bib
TMPFILE=temp.bib
if [ -f $OUTFILE ]
  then
    rm $OUTFILE
    echo deleted $OUTFILE
fi
cp $INFILE $TMPFILE
echo loaded $INFILE
#sed '/doi/d' -i $TMPFILE
sed '/url/d' -i $TMPFILE
sed '/note/d' -i $TMPFILE
cp $TMPFILE $OUTFILE
echo created $OUTFILE
rm $TMPFILE
